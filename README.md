# mysubnet
Check IP from subnet format list (x.x.x.x/24) in CSV file
## How to use in linux

./mysubnet-[version] [path-file.csv] [column-subnet] [check-ip-separated-by-comma]

```
mysubnet-1.0 sample.csv 1 10.38.37.1,10.58.5.32,10.58.5.33

````

![](img/windows.png)



### How to use in windows

mysubnet-[version].exe [path-file.csv] [column-subnet] [check-ip-separated-by-comma]

```
mysubnet-1.0.exe sample.csv 1 10.38.37.1,10.58.5.32,10.58.5.33

````

![](img/linux.png)
