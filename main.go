package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
)

func main() {
	// open file
	if len(os.Args) < 2 {
		fmt.Println("please use <file> <column-subnet> <ip-to-check>")
		os.Exit(0)

	}

	myFile := os.Args[1]
	mySubnet := os.Args[2]
	myIP := os.Args[3]
	mySubnetCol, _ := strconv.Atoi(mySubnet)

	// remember to close the file at the end of the program

	myIPs := strings.Split(myIP, ",")
	for k, v := range myIPs {
		fmt.Println("============", k, v)
		// read csv values using csv.Reader
		f, err := os.Open(myFile)
		if err != nil {
			log.Fatal(err)
		}
		csvReader := csv.NewReader(f)
		defer f.Close()

		i := 0
		exist := false

		for {
			rec, err := csvReader.Read()
			if err == io.EOF {
				break
			}

			if len(rec) < mySubnetCol {
				continue
			}
			//check CIDR
			network := strings.TrimSpace(rec[mySubnetCol])
			//fmt.Println(k, "myIP", v, i, network)
			i++
			_, subnet, errCIDR := net.ParseCIDR(network)
			if errCIDR != nil {
				//fmt.Println("error", errCIDR.Error())
				continue
			}

			ip := net.ParseIP(v)
			if subnet.Contains(ip) {
				//fmt.Println("myIP", v, rec)
				fmt.Println("myIP", v, rec)
				exist = true
				break
			}

			if err != nil {
				fmt.Println("error", err.Error())
				//log.Fatal(err)
			}

		}
		fmt.Println("found", exist)
	}

}
